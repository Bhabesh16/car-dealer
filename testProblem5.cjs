const inventory = require('./carInventory.cjs');
const getCarYearList = require('./problem4.cjs');
const getOldYearCarList = require('./problem5.cjs');

const carYearList = getCarYearList(inventory.carInventory);

const year = 2000;

const oldYearCarList = getOldYearCarList(carYearList, year);
console.log(oldYearCarList.length)