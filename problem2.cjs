function getLastCarInventoryInfo(carInventory) {
    if (carInventory === undefined || carInventory[carInventory.length - 1] === undefined || typeof carInventory === 'string') {
        return;
    }
    const lastCarInventoryInfo = carInventory[carInventory.length - 1];
    console.log(`Last car is a ${lastCarInventoryInfo.car_make} ${lastCarInventoryInfo.car_model}`);
}

module.exports = getLastCarInventoryInfo;