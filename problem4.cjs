function getCarYearList(carInventory) {
    if (carInventory === undefined || carInventory[carInventory.length - 1] === undefined || typeof carInventory === 'string') {
        return [];
    }
    let carYearList = [];
    for (let idx = 0; idx < carInventory.length; idx++) {
        carYearList.push(carInventory[idx].car_year);
    }
    return carYearList;
}

module.exports = getCarYearList;