const inventory = require('./carInventory.cjs');
const getChoiceCarList = require('./problem6.cjs');

const carMakeList = ["Audi", "BMW"];

const choiceCarList = getChoiceCarList(inventory.carInventory, carMakeList);

console.log(JSON.stringify(choiceCarList));