const inventory = require('./carInventory.cjs');
const getCarInformation = require('./problem1.cjs');

const carId = 33;
const carInfo = getCarInformation(inventory.carInventory, carId);

JSON.stringify(carInfo) !== '[]' ?
    console.log(`Car ${carInfo.id} is a ${carInfo.car_year} ${carInfo.car_make} ${carInfo.car_model}`) :
    null;