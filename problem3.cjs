function getCarModelList(carInventory) {
    if (carInventory === undefined || carInventory[carInventory.length - 1] === undefined || typeof carInventory === 'string') {
        return [];
    }
    let carModelList = [];
    for (let idx = 0; idx < carInventory.length; idx++) {
        carModelList.push(carInventory[idx].car_model);
    }
    carModelList.sort();
    return carModelList;
}

module.exports = getCarModelList;