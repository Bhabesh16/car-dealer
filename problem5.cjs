function getOldYearCarList(carYearList, year) {
    if (carYearList === undefined || typeof year === 'string' || year === undefined || carYearList[carYearList.length - 1] === undefined || typeof carYearList === 'string') {
        return [];
    }
    let oldYearCarList = [];
    for (let yearIdx = 0; yearIdx < carYearList.length; yearIdx++) {
        if (carYearList[yearIdx] < year) {
            oldYearCarList.push(carYearList[yearIdx]);
        }
    }
    return oldYearCarList;
}

module.exports = getOldYearCarList;