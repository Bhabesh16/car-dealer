function getChoiceCarList(carInventory, carMakeList) {
    if (carInventory === undefined || carInventory.length === 0 || carMakeList === undefined || carMakeList.length === 0) {
        return [];
    }

    const choiceCarList = [];
    for (let idx = 0; idx < carInventory.length; idx++) {
        for (let makeIdx = 0; makeIdx < carMakeList.length; makeIdx++) {
            if (carMakeList[makeIdx] == carInventory[idx].car_make) {
                choiceCarList.push(carInventory[idx]);
            }
        }
    }
    
    return choiceCarList;
}

module.exports = getChoiceCarList;